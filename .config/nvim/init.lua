--------------------------------------------------------------------------------
-- Plugins                                                                    --
--------------------------------------------------------------------------------
-- Install the plugins
function NVimInitInstallPlugins(use)
    -- The plugin manager itself
    use { 'wbthomason/packer.nvim' }

    -- Better parser, allow better syntax highlight
    use { 'nvim-treesitter/nvim-treesitter',
            run = function()
                local inst = require('nvim-treesitter.install')
                local ts_update = inst.update(
                    { with_sync = true }
                )
                ts_update()
            end,
    }

    -- A status line
    use {
        'nvim-lualine/lualine.nvim',
        requires = {
            'nvim-tree/nvim-web-devicons',
            opt = true
        }
    }

    -- Configuration for most LSPs
    use { 'neovim/nvim-lspconfig' }

    -- Eye candy based on LSPs
    use {
        'nvimdev/lspsaga.nvim',
        requires = {
            { 'nvim-tree/nvim-web-devicons', opt = true },
            { 'nvim-treesitter/nvim-treesitter', opt = true }
        }
    }

    -- Finder/Picker with fuzzy features
    use {
        'nvim-telescope/telescope.nvim',
        tag = '0.1.6',
        requires = {
            { 'nvim-lua/plenary.nvim', opt = false },
            { 'nvim-tree/nvim-web-devicons', opt = true },
            { 'nvim-treesitter/nvim-treesitter', opt = true }
        }
    }

    -- Addon for telescope to use a natif fzf implementation
    use {
        'nvim-telescope/telescope-fzf-native.nvim',
        run = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release &&'..
              'cmake --build build --config Release &&'..
              'cmake --install build --prefix build'
    }

    -- Completions
    use {
        'hrsh7th/nvim-cmp',
        requires = {
            { 'hrsh7th/cmp-nvim-lsp' },
            { 'hrsh7th/cmp-buffer' },
            { 'hrsh7th/cmp-path' },
            -- { 'hrsh7th/cmp-cmdline' },
            { 'hrsh7th/cmp-emoji' },
            { 'hrsh7th/cmp-vsnip' },
            { 'hrsh7th/vim-vsnip' }
        }
    }

    -- Debugger
    use {
        'rcarriga/nvim-dap-ui',
        requires = {
            { 'mfussenegger/nvim-dap' },
            { 'nvim-neotest/nvim-nio' },
            { 'theHamsta/nvim-dap-virtual-text' }
        }
    }

    -- Configure custom launchables tasks
    use { 'Shatur/neovim-tasks' }

    -- A file explorer treeview
    use { 'nvim-tree/nvim-tree.lua' }

    -- Dim inactive splits
    use { 'sunjon/shade.nvim' }

    -- Display git indicators in the gutter
    use { 'lewis6991/gitsigns.nvim' }

    -- Display various indicators in scroll bars
    use { 'petertriho/nvim-scrollbar' }

    -- Change line numbering to relative when it makes sense
    use { 'nkakouros-original/numbers.nvim' }

    -- A theme
    use { 'folke/tokyonight.nvim' }
end

-- Configure the plugins
function NVimInitConfigurePlugins()
    -- treesitter
    local treesitter = require('nvim-treesitter.configs')
    treesitter.setup({
        modules = {},
        sync_install = true,
        ensure_installed = { 'c', 'cpp', 'lua', 'vim', 'vimdoc', 'query' },
        auto_install = true,
        ignore_install = {},
        highlight = {
            enable = true
        }
    })

    -- lualine -----------------------------------------------------------------
    require('lualine').setup({})

    -- lspsaga -----------------------------------------------------------------
    require('lspsaga').setup({
        code_action = {
            keys = {
                quit = '<ESC>'
            },
            extend_gitsigns = true
        },
        callhierarchy = {
            keys = {
                close = '<ESC>'
            }
        },
        finder = {
            keys = {
                close = '<ESC>'
            }
        },
        lightbulb = {
            sign = false
        }
    })

    -- telescope ---------------------------------------------------------------
    local telescope = require('telescope')
    telescope.setup({})
    telescope.load_extension('fzf')

    -- nvim-cmp ----------------------------------------------------------------
    local cmp = require('cmp')
    local kind_icons = {
        Text = "",
        Method = "󰆧",
        Function = "󰊕",
        Constructor = "",
        Field = "󰇽",
        Variable = "󰂡",
        Class = "󰠱",
        Interface = "",
        Module = "",
        Property = "󰜢",
        Unit = "",
        Value = "󰎠",
        Enum = "",
        Keyword = "󰌋",
        Snippet = "",
        Color = "󰏘",
        File = "󰈙",
        Reference = "",
        Folder = "󰉋",
        EnumMember = "",
        Constant = "󰏿",
        Struct = "",
        Event = "",
        Operator = "󰆕",
        TypeParameter = "󰅲",
    }
    cmp.setup({
        formatting = {
            format = function(_, itm)
                itm.kind = (kind_icons[itm.kind] or "") .. ' ' .. itm.kind
                -- Trim the last ~ chars if present
                if itm.abbr:endswith('~') then
                    itm.abbr = itm.abbr:sub(1, -2)
                end
                return itm
            end
        },
        snippet = {
            expand = function(args)
                vim.fn["vsnip#anonymous"](args.body)
            end,
        },
        window = {
            completion = cmp.config.window.bordered(),
            documentation = cmp.config.window.bordered()
        },
        mapping = cmp.mapping.preset.insert({
            ['<C-Space>'] = cmp.mapping.complete(),
            ['<C-e>'] = cmp.mapping.abort(),
            ['<Tab>'] = cmp.mapping.confirm({ select = true }),
        }),
        sources = cmp.config.sources({
            { name = 'nvim_lsp' },
            { name = 'vsnip' },
            { name = 'buffer' },
            { name = 'path' },
            -- { name = 'cmdline' },
            { name = 'emoji' },
        }),
        sorting = {
            comparators = {
                cmp.config.compare.kind,
                cmp.config.compare.exact,
            }
        }
    })

    -- gray
    vim.api.nvim_set_hl(0, 'CmpItemAbbrDeprecated', { bg='NONE', strikethrough=true, fg='#808080' })
    -- blue
    vim.api.nvim_set_hl(0, 'CmpItemAbbrMatch', { bg='NONE', fg='#569CD6' })
    vim.api.nvim_set_hl(0, 'CmpItemAbbrMatchFuzzy', { link='CmpIntemAbbrMatch' })
    -- light blue
    vim.api.nvim_set_hl(0, 'CmpItemKindVariable', { bg='NONE', fg='#9CDCFE' })
    vim.api.nvim_set_hl(0, 'CmpItemKindInterface', { link='CmpItemKindVariable' })
    vim.api.nvim_set_hl(0, 'CmpItemKindText', { link='CmpItemKindVariable' })
    -- pink
    vim.api.nvim_set_hl(0, 'CmpItemKindFunction', { bg='NONE', fg='#C586C0' })
    vim.api.nvim_set_hl(0, 'CmpItemKindMethod', { link='CmpItemKindFunction' })
    -- front
    vim.api.nvim_set_hl(0, 'CmpItemKindKeyword', { bg='NONE', fg='#D4D4D4' })
    vim.api.nvim_set_hl(0, 'CmpItemKindProperty', { link='CmpItemKindKeyword' })
    vim.api.nvim_set_hl(0, 'CmpItemKindUnit', { link='CmpItemKindKeyword' })

    -- lspconfig ---------------------------------------------------------------
    local lspconfig = require('lspconfig')
    lspconfig.bashls.setup({})
    lspconfig.clangd.setup({
        capabilities = require('cmp_nvim_lsp').default_capabilities(),
        cmd = {
            "clangd",
            "--compile-commands-dir=build",
            "--background-index=true",
            "--clang-tidy",
            "--completion-style=detailed",
            "--enable-config",
            "--header-insertion=iwyu",
            "--header-insertion-decorators",
            "--log=error"
        }
    })
    lspconfig.jedi_language_server.setup({})
    lspconfig.lua_ls.setup({
        settings = {
            Lua = {
                runtime = {
                    -- Tell the language server which version of Lua you're using
                    -- (most likely LuaJIT in the case of Neovim)
                    version = 'LuaJIT',
                },
                diagnostics = {
                    -- Get the language server to recognize the `vim` global
                    globals = {
                        'vim',
                        'require'
                    },
                },
                workspace = {
                    -- Make the server aware of Neovim runtime files
                    library = vim.api.nvim_get_runtime_file("", true),
                },
                -- Do not send telemetry data containing a randomized but unique identifier
                telemetry = {
                    enable = false,
                },
            },
        },
    })
    vim.api.nvim_create_autocmd('LspAttach', {
        group = vim.api.nvim_create_augroup('UserLspConfig', {}),
        callback = function(ev)
            -- Enable completion triggered by <c-x><c-o>
            vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'
        end,
    })


    -- tasks -------------------------------------------------------------------
    require('tasks').setup({
        -- Default module parameters with which `neovim.json` will be created.
        default_params = {
            cmake = {
                cmd = 'cmake',
                build_dir = tostring(require('plenary.path'):new('{cwd}', 'build')),
                build_type = 'Debug',
                dap_name = 'lldb',
                args = {
                    configure = { '-D', 'CMAKE_EXPORT_COMPILE_COMMANDS=1', '-G', 'Ninja' },
                },
            },
        },
        save_before_run = true,
        params_file = 'neovim.json',
        quickfix = {
            pos = 'botright',
            height = 12,
        },
        dap_open_command = function() return require('dap').repl.open() end,
    })

    -- dap ---------------------------------------------------------------------
    local dap = require('dap')

    -- Adapters: gdb
    dap.adapters.gdb = {
        type = "executable",
        command = "gdb",
        args = { "-i", "dap" }
    }

    -- Configurations:
    dap.configurations.c = {
        {
            name = "Launch",
            type = "gdb",
            request = "launch",
            program = function()
                return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/build/', 'file')
            end,
            cwd = "${workspaceFolder}",
            stopAtBeginningOfMainSubprogram = false,
        },
    }
    dap.configurations.cpp = dap.configurations.c
    vim.fn.sign_define('DapBreakpoint', {text='🔴', texthl='DapBreakpoint', linehl='DapBreakpoint', numhl='DapBreakpoint'})

    -- dapui -------------------------------------------------------------------
    local dapui = require('dapui')
    dapui.setup()
    dap.listeners.before.attach.dapui_config = function()
        dapui.open()
    end
    dap.listeners.before.launch.dapui_config = function()
        dapui.open()
    end
    dap.listeners.before.event_terminated.dapui_config = function()
        dapui.close()
    end
    dap.listeners.before.event_exited.dapui_config = function()
        dapui.close()
    end

    -- nvim-dap-virtual-text ---------------------------------------------------
    require("nvim-dap-virtual-text").setup({})

    -- nvim-tree ---------------------------------------------------------------
    require('nvim-tree').setup()

    -- shade -------------------------------------------------------------------
    require('shade').setup({
        overlay_opacity = 70
    })

   -- gitsigns ----------------------------------------------------------------
    require('gitsigns').setup()

    -- scrollbar ---------------------------------------------------------------
    require("scrollbar").setup()

    -- numbers -----------------------------------------------------------------
    require('numbers').setup()

    -- tokyonight --------------------------------------------------------------
    require("tokyonight").setup({ style = "night" })
end

--------------------------------------------------------------------------------
-- Main configuration                                                         --
--------------------------------------------------------------------------------
function NVimInitConfigureGlobal()
    vim.g.loaded_netrw          = 1             -- Disable netrw
    vim.g.loaded_netrwPlugin    = 1             -- Disable netrw plugin
    vim.opt.termguicolors       = true          -- Enable highlight groups
    vim.opt.modeline            = true          -- Enable the modeline support
    vim.opt.modelines           = 3             -- The modeline must be in the first or last 3 lines
    vim.opt.number              = true          -- Display line numbers
    vim.opt.relativenumber      = true          -- Relative line numbering
    vim.opt.wrap                = false         -- Disable wrapping
    vim.opt.wrapmargin          = 0             -- Disable wrap margin
    vim.opt.textwidth           = 0             -- Disable auto line breaking
    vim.opt.tabstop             = 4             -- A tab count for 4 spaces 
    vim.opt.softtabstop         = 4             -- A tab count for 4 spaces
    vim.opt.shiftwidth          = 4             -- A tab count for 4 spaces 
    vim.opt.expandtab           = true          -- Expand tabs to spaces
    vim.opt.scrolloff           = 5             -- Keep at least 5 lines before and after cursor when scrolling
    vim.opt.splitright          = true          -- Vertical split on right by default
    vim.opt.splitbelow          = true          -- Horizontal split on below by default
    vim.opt.colorcolumn         = "80,120"      -- Show a column at 80 and 120 for breaking hints
    vim.opt.clipboard           = "unnamedplus" -- Use the X clipboard by default
    vim.opt.wildignore:append {
        '*/.git/*',
        '*/build/*',
        '*.o',
        '*~',
        '*.pyc'
    }
    vim.opt.listchars = {
        space = '·',
        tab = '⤚⎼🠢',
        trail = '␣'
    }
    vim.opt.list                = true          -- Show the listchars

    -- ColorScheme customizations
    vim.api.nvim_set_hl(0, 'ColorColumn', { ctermbg=250 }) -- Must be before tokyonight
    vim.cmd.colorscheme('tokyonight-night')        -- Set the theme

    -- Other options
    vim.lsp.set_log_level("off")
end

--------------------------------------------------------------------------------
-- Keymapping                                                                 --
--------------------------------------------------------------------------------
function NVimInitConfigureKeymaps()
    local telescope = require('telescope.builtin')
    local dapui = require('dapui')

    -- Use space as the leader key
    vim.g.mapleader = ' '

    -- ALT+up: move current line up
    vim.keymap.set('n', '<A-up>', '<cmd>m.-2<CR>==', {noremap = true})
    vim.keymap.set('v', '<A-up>', '<cmd>m\'<-2<CR>gv=gv', {noremap = true})
    vim.keymap.set('i', '<A-up>', '<Esc><cmd>m .-2<CR>==gi', {noremap = true})
    -- ALT+down: move current line up
    vim.keymap.set('n', "<A-down>", '<cmd>m.+1<CR>==', {noremap = true})
    vim.keymap.set('v', "<A-down>", '<cmd>m\'<+1<CR>gv=gv', {noremap = true})
    vim.keymap.set('i', "<A-down>", '<Esc><cmd>m .+1<CR>==gi', {noremap = true})
    -- Insert to go in Insert mode, ALT+Insert for replace mode
    vim.keymap.set('i', '<Insert>', '', {remap = true})
    vim.keymap.set('i', '<A-Insert>', '<Insert>', {noremap = true})
    vim.keymap.set('n', '<A-Insert>', '<Insert><Insert>', {noremap = true})
    -- Exit terminal with ESC
    vim.keymap.set('t', '<ESC>', '<C-\\><C-n>', {noremap = true})

    -- Diagnostic mappings
    vim.keymap.set('n', '<leader>ds', vim.diagnostic.open_float)
    vim.keymap.set('n', '<leader>do', vim.diagnostic.setloclist)
    vim.keymap.set('n', '<leader>dn', vim.diagnostic.goto_prev)
    vim.keymap.set('n', '<leader>dp', vim.diagnostic.goto_next)

    -- Code actions
    vim.keymap.set('n', '<leader>cA', vim.lsp.buf.code_action)

    -- Find (with and without fuzzy)
    vim.keymap.set('n', '<leader>fb', telescope.buffers)
    vim.keymap.set('n', '<leader>fc', vim.lsp.buf.incoming_calls)
    vim.keymap.set('n', '<leader>fC', vim.lsp.buf.outgoing_calls)
    vim.keymap.set('n', '<leader>fd', telescope.diagnostics, { noremap = true, silent = true })
    vim.keymap.set('n', '<leader>ff', telescope.find_files)
    vim.keymap.set('n', '<leader>fg', telescope.live_grep)
    vim.keymap.set('n', '<leader>fh', telescope.help_tags)
    vim.keymap.set('n', '<leader>fi', vim.lsp.buf.implementation)
    vim.keymap.set('n', '<leader>fr', vim.lsp.buf.references)
    vim.keymap.set('n', '<leader>fR', telescope.lsp_references)
    vim.keymap.set('n', '<leader>fs', telescope.current_buffer_fuzzy_find)

    -- LspSaga
    vim.keymap.set('n', '<leader>sa', '<cmd>Lspsaga code_action<CR>')
    vim.keymap.set('n', '<leader>sc', '<cmd>Lspsaga incoming_calls<CR>')
    vim.keymap.set('n', '<leader>sC', '<cmd>Lspsaga outgoing_calls<CR>')
    vim.keymap.set('n', '<leader>sd', '<cmd>Lspsaga peek_definition<CR>')
    vim.keymap.set('n', '<leader>st', '<cmd>Lspsaga peek_type_definition<CR>')
    vim.keymap.set('n', '<leader>sf', '<cmd>Lspsaga finder<CR>')
    vim.keymap.set('n', '<leader>sk', '<cmd>Lspsaga hover_doc<CR>')
    vim.keymap.set('n', '<leader>sK', '<cmd>Lspsaga hover_doc ++keep<CR>')
    vim.keymap.set('n', '<leader>so', '<cmd>Lspsaga outline<CR>')

    -- Goto mappings
    vim.keymap.set('n', '<leader>gd', vim.lsp.buf.definition)
    vim.keymap.set('n', '<leader>gD', vim.lsp.buf.declaration)
    vim.keymap.set('n', '<leader>gt', vim.lsp.buf.type_definition)

    -- Intellisense mappings
    vim.keymap.set('n', '<leader>k', vim.lsp.buf.hover)
    vim.keymap.set('n', '<leader>K', vim.lsp.buf.signature_help)

    -- Refactore mappings
    vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename)
    vim.keymap.set('n', '<leader>bf', vim.lsp.buf.format)

    -- Workspace mappings
    vim.keymap.set('n', '<leader>wa', vim.lsp.buf.add_workspace_folder)
    vim.keymap.set('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder)
    vim.keymap.set('n', '<leader>wl', function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, {})

    -- Treeview
    local nvtapi = require('nvim-tree.api').tree
    vim.keymap.set('n', '<leader>tf', nvtapi.find_file, {})
    vim.keymap.set('n', '<leader>tt', nvtapi.toggle, {})

    -- Debugging
    vim.keymap.set('n', '<leader>dt', dapui.toggle, {noremap = true})
    vim.keymap.set('n', '<leader>db', ':DapToggleBreakpoint<CR>', {noremap = true})
    vim.keymap.set('n', '<leader>dc', ':DapContinue<CR>', {noremap = true})
    vim.keymap.set('n', '<leader>dr', ':lua require(\'dapui\').open{reset = true})<CR>', {noremap = true})
    vim.keymap.set('n', '<leader>dr', function() dapui.close(); dapui.open({reset = true}) end, {noremap = true})

    -- External tools ----------------------------------------------------------
    vim.keymap.set('n', '<leader>ma', ':make -j all<CR>', {noremap = true})
    vim.keymap.set('n', '<leader>mc', ':make -j compile_commands.json<CR>', {noremap = true})
    vim.keymap.set('n', '<leader>cc', function() require('tasks').start('cmake', 'configure') end, {})
    vim.keymap.set('n', '<leader>cb', function() require('tasks').start('cmake', 'build') end, {})
    vim.keymap.set('n', '<leader>kf', ':%!clang-format<CR>', {noremap = true})

end

function NVimInitConfigure()
    NVimInitConfigurePlugins()
    NVimInitConfigureGlobal()
    NVimInitConfigureKeymaps()
end

--------------------------------------------------------------------------------
--Functions                                                                   --
--------------------------------------------------------------------------------
-- Install the plugin manager if not already
function NVimInitPMInstall()
    local fn = vim.fn
    local pm_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(pm_path)) > 0 then
        fn.system({
            'git', 'clone', '--depth', '1',
            'https://github.com/wbthomason/packer.nvim',
            pm_path
        })
        vim.cmd.packadd('packer.nvim')
        return true
    end
    return false
end

function NVimInitMain()
    -- Install the plugin manager
    local newinstall = NVimInitPMInstall()
    local packer = require('packer')
    return packer.startup(
        function(use)
            NVimInitInstallPlugins(use)
            if newinstall then
                vim.api.nvim_create_augroup(
                    'packer_user_config',
                    { clear = true }
                )
                vim.api.nvim_create_autocmd(
                    'User',
                    {
                        pattern = 'PackerComplete',
                        command = 'lua nvim_init_configure()'
                    }
                )
                packer.sync()
            else
                NVimInitConfigure()
            end
        end
    )
end

function string:endswith(ending)
    return ending == "" or self:sub(-#ending) == ending
end

--------------------------------------------------------------------------------
-- Entry point                                                                --
--------------------------------------------------------------------------------
return NVimInitMain()
