command -v bat &> /dev/null && alias cat='bat --theme="Monokai Extended"'

alias grep='grep --color=auto'

if command -v eza &> /dev/null; then
    alias ls='eza --group-directories-first --icons=auto'
    alias ll='eza -aghlm@ --time-style long-iso --git --group-directories-first --icons=auto'
elif command -v exa &> /dev/null; then
    alias ls='exa --group-directories-first --icons=auto'
    alias ll='exa -aghlm@ --time-style long-iso --git --group-directories-first --icons=auto'
else
    alias ls='ls --color=auto'
    alias ll='ls --color=auto -lah'
fi
