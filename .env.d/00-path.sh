path_prepend() {
    if [ -d "${1}" ]; then
        if ! echo ":${PATH}:" | grep ":${1}:" > /dev/null 2>&1; then
            PATH="${1}:${PATH}"
        fi
     fi
}

path_append() {
    if [ -d "${1}" ]; then
        if ! echo ":${PATH}:" | grep ":${1}:" > /dev/null 2>&1; then
            PATH="${PATH}:${1}"
        fi
    fi
}

path_prepend "${HOME}/.local/bin"

export PATH
