# dotfiles

This repository holds some of the common configuration files I use.

## Installation


First, clone the repository somewhere and copy/paste files to your home directory.

```
git clone git@gitlab.com:ctxnop/dotfiles.git
cp dotfiles/.zshrc ~/
```

I personnaly clone it into `~/.dotfiles` and use symlinks.

```
git clone git@gitlab.com:ctxnop/dotfiles.git ~/.dotfiles
ls ~/.dotfiles/.zshrc ~/.zshrc
```

## Git configuration

The provided `~/.gitconfig` is almost empty and instead includes files from `~/.config/git`.
It also includes the `~/.config/git/identity` file which isn't provided.
That file is ignored by the provided `.gitignore` so that there is no risk to commit it by mistake.
Just add your confidential configuration to it (`user.name`, `user.email`, `user.signingkey`, etc).
